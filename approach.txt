Problem Statement:

To predict the customer lifetime value based on the user data and policy data.

Approach:

Data was loaded as pandas dataframe
categorical columns were encoded using ordinal encoding.
XGBoost model was used
FLAML - AutoML library was used for hyperparameter tuning 
Final predictions were made using model trained with Best hyperparmeter config.